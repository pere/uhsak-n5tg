all:

pdf: issues.pdf

issues.pdf: issues.rst
	pandoc $^ -o $@

issues.html: issues.rst
	pandoc $^ -o $@

issues.rst: issues/*.yaml
	./scripts/issues2rst issues/*.yaml > $@

clean:
	$(RM) *~ */*~
